import { GlobalStyles } from 'styles/global'
import Routes from 'routes'

const App = () => {
  return (
    <>
      <Routes />
      <GlobalStyles />
    </>
  )
}

export default App
