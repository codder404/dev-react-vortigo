import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import {
  Home,
  SplashScreen,
  SignUp,
  Selected,
  SelectPokemon,
  NotFound
} from 'pages'

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={SplashScreen} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/cadastro" component={SignUp} />
        <Route exact path="/cadastro/pokemon" component={SelectPokemon} />
        <Route exact path="/selected" component={Selected} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  )
}

export default Routes
