import styled from 'styled-components'

export const Slider = styled.section`
  margin-top: 2rem;

  > ul {
    align-items: center;
    display: flex;
  }

  > ul li {
    align-items: center;
    display: flex;
    flex-direction: column;
    margin-right: 1rem;

    > img {
      border-radius: 50%;
    }

    > span {
      color: #222;
      font-size: 1.5rem;
      font-weight: 700;
      margin-top: 1rem;
    }
  }
`
