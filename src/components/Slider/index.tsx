import { useEffect, useState } from 'react'

import { Loading } from 'components'
import api from 'services'
import * as S from './slider.styles'

const Slider = () => {
  const [data, setData] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const getPokemon = async () => {
      setIsLoading(true)
      const response = await api.get('/')
      setData(response.data)
      setIsLoading(false)
    }

    getPokemon()
  }, [])

  return (
    <S.Slider>
      {isLoading ? (
        <Loading>Loading...</Loading>
      ) : (
        <ul>
          {data.map(({ name, thumbnailImage }) => (
            <li key={name}>
              <img src={thumbnailImage} alt={name} />
              <span>{name}</span>
            </li>
          ))}
        </ul>
      )}
    </S.Slider>
  )
}

export default Slider
