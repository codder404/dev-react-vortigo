import styled from 'styled-components'
import { theme } from 'styles/global'

export const Button = styled.button`
  background-color: ${theme.color.secondary};
  border-radius: 0.5rem;
  color: ${theme.color.white};
  cursor: pointer;
  font-size: ${theme.fonts.normal};
  font-weight: 700;
  padding: 1.5rem;
  text-align: center;
  width: 24rem;
`
