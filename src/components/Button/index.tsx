import * as S from './button.styles'

type ButtonProps = {
  children: React.ReactNode
}

const Button = ({ children }: ButtonProps) => {
  return (
    <S.Button role="role" type="button">
      {children}
    </S.Button>
  )
}

export default Button
