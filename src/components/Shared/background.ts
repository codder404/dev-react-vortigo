import styled from 'styled-components'
import BackgroundImg from 'assets/images/bg@3x.png'

const BGWrapper = styled.div`
  background-image: url(${BackgroundImg});
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 100vh;
`
export default BGWrapper
