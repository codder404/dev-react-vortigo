import styled from 'styled-components'

import { theme } from 'styles/global'

const Wrapper = styled.div`
  color: ${theme.color.secondary};
  font-size: 1.85rem;
  font-weight: 700;
  text-align: center;
  margin-top: 2rem;
`

type LoadingProps = {
  children: React.ReactNode
}

const Loading = ({ children }: LoadingProps) => {
  return <Wrapper>{children}</Wrapper>
}

export default Loading
