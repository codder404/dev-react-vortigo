import { Link } from 'react-router-dom'
import styled from 'styled-components'

export const NextLink = styled(Link)`
  display: flex;
  justify-content: center;
  margin-top: 12rem;

  > img {
    height: 8rem;
  }
`
