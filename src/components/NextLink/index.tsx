import * as S from './nextlink.styles'

type LinkProps = {
  children: React.ReactNode
  link: string
}

const Link = ({ children, link }: LinkProps) => {
  return <S.NextLink to={link}>{children}</S.NextLink>
}

export default Link
