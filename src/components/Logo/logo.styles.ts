import styled from 'styled-components'

export const Wrapper = styled.div`
  > img {
    padding-top: 1.8rem;
    margin-bottom: -1rem;
  }
`
export const Divider = styled.div`
  display: flex;
  justify-content: center;

  > img {
    height: 5rem;
  }
`
