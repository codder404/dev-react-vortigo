import LogoImg from 'assets/images/pokemon-logo@3x.png'
import FinderImg from 'assets/images/finder@3x.png'

import * as S from './logo.styles'

const Logo = () => {
  return (
    <S.Wrapper>
      <img role="img" src={LogoImg} alt="Pokemon Logo" />
      <S.Divider>
        <img role="img" src={FinderImg} alt="Finder Logo" />
      </S.Divider>
    </S.Wrapper>
  )
}

export default Logo
