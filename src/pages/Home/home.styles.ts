import styled from 'styled-components'
import { theme } from 'styles/global'

export const Wrapper = styled.main`
  > div {
    text-align: center;
    > a {
      color: ${theme.color.secondary};
      font-size: 1.5rem;
      text-decoration: underline;
    }
  }
`
export const Header = styled.header`
  align-items: center;
  background-color: ${theme.color.primary};
  display: flex;
  justify-content: center;
  flex-direction: column;
  height: 10rem;

  > span {
    color: ${theme.color.white};
    font-size: ${theme.fonts.large};
    font-weight: 500;
  }

  .search-input {
    align-items: center;
    display: flex;
    justify-content: space-between;
    height: 60px;
    width: 300px;

    > svg {
      position: relative;
      left: 3rem;
      font-size: 3rem;
    }

    > input {
      color: #222;
      border: none;
      border-radius: 0.4rem;
      font-size: 1.5rem;
      height: 35px;
      padding-left: 50px;
      width: 375px;
    }
  }
`
export const SortedBy = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  margin-top: 4rem;

  p {
    color: #222;
    font-size: 1.5rem;
    font-weight: 400;
  }

  select {
    border: none;
  }
`
export const PokemonList = styled.div`
  margin-bottom: 1rem;

  > ul li {
    align-items: center;
    display: flex;

    > img {
      border-radius: 50%;
      height: 7rem;
    }

    > span {
      margin-left: 1.2rem;
    }
  }
`
