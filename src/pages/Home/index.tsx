/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from 'react'
import { FaSearch } from 'react-icons/fa'
import axios from 'axios'

import { Container, Loading, Slider } from 'components'
import * as S from './home.styles'

const Home = () => {
  const [name, setName] = useState('')
  const [foundPokemons, setFoundPokemons] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true)
      const response = await axios(
        'https://vortigo.blob.core.windows.net/files/pokemon/data/pokemons.json'
      )

      setFoundPokemons(response.data)
      setIsLoading(false)
    }

    fetchData()
  }, [])

  const filter = (e: any) => {
    const keyword = e.target.value

    if (keyword !== '') {
      const results = foundPokemons.filter((item: any) => {
        return item.name.toLowerCase().startsWith(keyword.toLowerCase())
      })

      setFoundPokemons(results)
    } else {
      setFoundPokemons(foundPokemons)
    }

    setName(keyword)
  }
  return (
    <S.Wrapper>
      <S.Header>
        <span>Pokemon Finder</span>
        <div className="search-input">
          <FaSearch />
          <input
            type="search"
            value={name}
            onChange={filter}
            placeholder="Search"
          />
        </div>
      </S.Header>
      <Container>
        <Slider />
        <S.SortedBy>
          <p>Pokémon</p>
          <select name="name" id="name">
            <option value="name">Name</option>
          </select>
        </S.SortedBy>
        {isLoading ? (
          <Loading>Loading...</Loading>
        ) : (
          <S.PokemonList>
            {foundPokemons && foundPokemons.length > 0 ? (
              foundPokemons.map(({ id, name, thumbnailAltText }) => (
                <ul key={id}>
                  <li>
                    <img src={thumbnailAltText} alt="Avatar" />
                    <span>{name}</span>
                  </li>
                </ul>
              ))
            ) : (
              <h1>No results found!</h1>
            )}
          </S.PokemonList>
        )}
        <a>Load more</a>
      </Container>
    </S.Wrapper>
  )
}

export default Home
