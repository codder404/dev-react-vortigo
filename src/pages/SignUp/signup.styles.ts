import styled from 'styled-components'
import { BGWrapper } from 'components'
import { theme } from 'styles/global'

export const Wrapper = styled(BGWrapper)`
  > div {
    padding-top: 12rem;

    .title,
    .subtitle {
      color: ${theme.color.white};
      font-size: 2rem;
      line-height: ${theme.lines.small};
    }

    .subtitle {
      margin-top: 12rem;
    }

    .input-box {
      border: 0;
      border-radius: 0.4rem;
      color: #222;
      margin-top: 10rem;
      padding: 0.7rem;
      width: 26rem;
    }
  }
`
