import { Container, NextLink } from 'components'
import * as S from './signup.styles'

import NextImg from 'assets/images/next@3x.png'

const SignUp = () => {
  return (
    <S.Wrapper>
      <Container>
        <h2 className="title">Let&apos;s meet each other first?</h2>
        <h3 className="subtitle">First we need to know your name...</h3>
        <input
          type="text"
          name="name"
          id="name"
          className="input-box"
          placeholder="Your name"
        />
        <NextLink link="/cadastro/pokemon">
          <img src={NextImg} alt="Next icon" />
        </NextLink>
      </Container>
    </S.Wrapper>
  )
}

export default SignUp
