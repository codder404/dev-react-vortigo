import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { BGWrapper } from 'components'
import { theme } from 'styles/global'

export const Wrapper = styled(BGWrapper)`
  > div {
    padding-top: 10rem;

    .title,
    .subtitle {
      color: ${theme.color.white};
      font-size: 2.5rem;
      line-height: ${theme.lines.small};
    }

    .title {
      margin-top: 1rem;
    }

    .subtitle {
      margin-top: 12rem;
      font-size: 1.6rem;
    }

    .select-box {
      border: 0;
      border-radius: 0.4rem;
      color: #222;
      margin-top: 10rem;
      padding: 0.7rem;
      width: 26rem;
    }
  }
`
export const Back = styled(Link)`
  align-items: center;
  display: flex;

  > svg {
    color: ${theme.color.white};
    height: 2rem;
    width: 3rem;
  }

  span {
    margin-left: 0.7rem;
    color: ${theme.color.white};
    font-size: 1.4rem;
  }
`
