import { Link } from 'react-router-dom'
import { FaArrowLeft } from 'react-icons/fa'
import { Container, NextLink } from 'components'
import * as S from './selectPokemon.styles'

import NextImg from 'assets/images/next@3x.png'

const SelectPokemon = () => {
  return (
    <S.Wrapper>
      <Container>
        <S.Back to="/cadastro">
          <FaArrowLeft />
          <span>Back</span>
        </S.Back>
        <h2 className="title">Hello, trainer Dev!</h2>
        <h3 className="subtitle">
          ...now tell us which is your favorite Pokémon type:
        </h3>
        <Link to="/selected">
          <select name="name" id="name" className="select-box" />
        </Link>
        <NextLink link="/">
          <img role="img" src={NextImg} alt="Next icon" />
        </NextLink>
      </Container>
    </S.Wrapper>
  )
}

export default SelectPokemon
