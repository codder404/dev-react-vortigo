import { Container, Button, Logo } from 'components'
import * as S from './splashScreen.styles'

import AvatarImg from 'assets/images/pikachu@3x.png'

const SplashScreen = () => {
  return (
    <S.Wrapper>
      <Container>
        <Logo />
        <S.LinkGoTo to="/cadastro">
          <Button>Let&apos;s go!</Button>
        </S.LinkGoTo>
      </Container>
      <S.ImageWrapper>
        <img src={AvatarImg} alt="Pikachu" />
      </S.ImageWrapper>
    </S.Wrapper>
  )
}

export default SplashScreen
