import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { BGWrapper } from 'components'

export const Wrapper = styled(BGWrapper)``
export const LinkGoTo = styled(Link)`
  display: flex;
  justify-content: center;
  margin-top: 12rem;
`
export const ImageWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 19.5rem;

  > img {
    height: 15rem;
  }
`
