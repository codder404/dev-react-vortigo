import { useEffect, useState } from 'react'
import { FaArrowLeft } from 'react-icons/fa'
import * as S from './selected.styles'
import { Button, Loading } from 'components'
import api from 'services'

const Selected = () => {
  const [data, setData] = useState([])
  const [isChecked, setIsChecked] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const fetchPokemon = async () => {
      setIsLoading(true)

      const response = await api.get('/')

      setData(response.data)
      setIsLoading(false)
    }
    fetchPokemon()
  }, [])

  function handleCheckedChange() {
    setIsChecked((prevCheckedValue) => !prevCheckedValue)
  }

  return (
    <>
      <S.Wrapper>
        <S.Back to="/cadastro/pokemon">
          <FaArrowLeft />
          <span>Back</span>
        </S.Back>
        <h2 className="title">Hello, trainer Dev!</h2>
      </S.Wrapper>
      <S.Container>
        <h1>Select your favorite pokémon type</h1>
        <div className="radio-list">
          {isLoading ? (
            <Loading>Loading...</Loading>
          ) : (
            <ul>
              {data.map(({ name, thumbnailImage }) => (
                <label htmlFor="name" key={name}>
                  <span>
                    <img src={thumbnailImage} alt={name} />
                    <h2>{name}</h2>
                  </span>
                  <input
                    type="radio"
                    checked={isChecked}
                    onChange={handleCheckedChange}
                    id="radiochecked"
                  />
                </label>
              ))}
            </ul>
          )}
        </div>
        <Button>Confirm</Button>
      </S.Container>
    </>
  )
}

export default Selected
