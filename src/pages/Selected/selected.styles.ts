import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { BGWrapper } from 'components'
import { theme } from 'styles/global'

export const Wrapper = styled(BGWrapper)`
  height: 35vh;
  padding: 4rem;
  > h2 {
    color: ${theme.color.white};
    font-size: 2.5rem;
    line-height: ${theme.lines.small};
    margin-top: 2rem;
  }
`
export const Container = styled.div`
  margin: 0 auto;
  margin-top: 4rem;
  text-align: center;
  width: 80%;

  h1 {
    color: #131313;
    font-size: 1.6rem;
    font-weight: 500;
    line-height: ${theme.lines.small};
  }

  .radio-list {
    margin-bottom: 1.4rem;
    > ul label {
      align-items: center;
      display: flex;
      justify-content: space-between;

      > input {
        height: 2rem;
        width: 2rem;
      }
    }

    span {
      align-items: center;
      display: flex;

      > img {
        border-radius: 50%;
        height: 6rem;
      }

      > h2 {
        font-size: 1.5rem;
        font-weight: 700;
      }
    }
  }
`
export const Back = styled(Link)`
  align-items: center;
  display: flex;

  > svg {
    color: ${theme.color.white};
    height: 2rem;
    width: 3rem;
  }

  span {
    margin-left: 0.7rem;
    color: ${theme.color.white};
    font-size: 1.4rem;
  }
`
