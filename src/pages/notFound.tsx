import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { BGWrapper } from 'components'

const Wrapper = styled(BGWrapper)`
  align-items: center;
  display: flex;
  justify-content: center;
  flex-direction: column;

  > h1 {
    color: #ffffff;
    font-weight: 700;
  }

  > p {
    color: #ffffff;
    font-size: 1.75rem;
    line-height: 2.5rem;
    text-align: center;
    width: 325px;
  }

  > a {
    background: #f11e76;
    border-radius: 0.4rem;
    color: #ffffff;
    font-size: 1.75rem;
    margin-top: 2.75rem;
    padding: 1.5rem 2.75rem;
  }
`

const NotFound = () => {
  return (
    <Wrapper>
      <h1>404</h1>
      <p>
        Ooooops! Sorry this page does not exist. May the Force be with You! 🖖
      </p>
      <Link to="/home">Back to home</Link>
    </Wrapper>
  )
}

export default NotFound
